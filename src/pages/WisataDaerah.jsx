import React from 'react';
import { Heading, Skeleton, Stack, Text, Image } from '@chakra-ui/react';
import { Link } from 'react-router-dom';

import Layout from '../components/layouts';
import Section from '../components/sections/Section';
import Banner from '../components/sections/Banner';

import wisataDaerahImages from '../assets/images/banner.png';
import data from '../assets/raw/wisata-daerah.json';

const WisataDaerah = ({ }) => {
  return (
    <Layout>
      <Banner
        title="Wisata Daerah"
        description="Beranda &nbsp; →  &nbsp; Wisata Daerah"
        image={wisataDaerahImages}
      />

      <Section>
        {data.map((item, index) =>
          <Stack
          mb={5}
            direction="row"
            shadow="rgba(0, 0, 0, 0) 0px 0.9px 3.2px 0px, rgba(0, 0, 0, 0.004) 0px 2.1px 7.1px 0px, rgba(0, 0, 0, 0.004) 0px 3.5px 12.1px 0px, rgba(0, 0, 0, 0.008) 0px 5.4px 18.7px 0px, rgba(0, 0, 0, 0.01) 0px 8.1px 27.7px 0px, rgba(0, 0, 0, 0.016) 0px 11.8px 40.7px 0px, rgba(0, 0, 0, 0.02) 0px 17.7px 60.9px 0px, rgba(0, 0, 0, 0.03) 0px 28.3px 97.1px 0px, rgba(0, 0, 0, 0.07) 0px 53px 182px 0px
        "
          display={{ md: "flex" }}
            borderRadius="md"
            p={8}
            spacing="2rem"
            mb="20px"
            ml="40px"
            mr="40px"
          >
            <Image
              src={item.image}
              objectFit="cover"
              width="150px"
              htmlWidth="150px"
              h="144px"
              borderRadius="md"
            />
            <Stack align="start" w="60%">
              <Heading fontSize="24px" fontWeight="extra_bold">
                {item.title}
              </Heading>
              <Text>{item.desc}</Text>
              <Link to={item.link}>
                <Text color="green.500">Selengkapnya</Text>
              </Link>
            </Stack>
          </Stack>
        )}
      </Section>
    </Layout>
  );
};

export default WisataDaerah;
