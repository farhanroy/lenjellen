import React from 'react';
import {
  Box,
  Heading,
  Image,
  Stack,
  Text,
  Badge,
  Icon,
  SimpleGrid,
  Center,
} from '@chakra-ui/react';

import {GrLocation} from 'react-icons/gr';
import {BiTime} from 'react-icons/bi';

import Layout from '../components/layouts';
import Banner from '../components/sections/Banner';
import eventKotaImages from '../assets/images/banner.png';
import Section from '../components/sections/Section';

const EventKota = props => {
  return (
    <Layout>
      <Banner
        title="Event Kota"
        description="Beranda &nbsp; →  &nbsp; Event Kota"
        image={eventKotaImages}
      />

      <Section>
        <Stack id="mulai-sekarang-target" textAlign="center" mt="48px" >
          <Text w="100%" fontWeight="extra_bold" color="brand.secondary">
            KEINDAHAN PASURUAN
          </Text>
          <Heading fontWeight="extra_bold">
            Keberagaman & Budaya Pasuruan
          </Heading>
          <Text className="col-md-7 mx-auto">
            Nikmati berbagai macam budaya, keberagaman, kuliner dan keindahan
            alam yang membuat-mu jatuh cinta dan menjadikan Pasuruan sebagai
            destinasi-mu selanjutnya
          </Text>
        </Stack>
        <div className="row mt-5">
          <div className="col-md-4">
          <AirbnbExample />
          </div>
          <div className="col-md-4">
          <AirbnbExample />
          </div>
          <div className="col-md-4">
          <AirbnbExample />
          </div>
          <div className="col-md-4">
          <AirbnbExample />
          </div>
        </div>
      </Section>

    </Layout>
  );
};

function AirbnbExample() {
  const property = {
    imageUrl: "https://i.ytimg.com/vi/NCbGIPQ5KoI/maxresdefault.jpg",
    imageAlt: "Rear view of modern home with pool",
    beds: 3,
    baths: 2,
    title: "Haul KH Abdul Hamid ke 40",
    formattedPrice: "$1,900.00",
    reviewCount: 34,
    rating: 4,
  }

  return (
    <Box maxW="sm" borderWidth="1px" borderRadius="lg" overflow="hidden" mt="10">
      <Image src={property.imageUrl} alt={property.imageAlt} width="100%" objectFit="cover" />

      <Box p="6">
        <Box display="flex" alignItems="baseline">
          <Badge borderRadius="full" colorScheme="teal">
            Soon
          </Badge>
          <Box
            color="gray.500"
            fontWeight="semibold"
            letterSpacing="wide"
            fontSize="xs"
            textTransform="uppercase"
            ml="2"
          >
            2 Hari Lagi
          </Box>
        </Box>

        <Box
          mt="4"
          fontWeight="semibold"
          as="h4"
          lineHeight="tight"
          isTruncated
        >
          {property.title}
        </Box>

        <Box mt="16px">
          <Icon
            color="brand.secondary"
            as={BiTime}
            mr="0.5rem"
            fontSize="lg"
          />
          <Box as="span" color="gray.600" fontSize="sm">
          08:00 - Selesai, 18 Oktober 2021
          </Box>
        </Box>

        <Box mt="10px">
          <Icon
            color="brand.secondary"
            as={GrLocation}
            mr="0.5rem"
            fontSize="lg"
          />
          <Box as="span" color="gray.600" fontSize="sm">
          Kebonsari, Kota Pasuruan
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default EventKota;