import React from 'react';
import {
  Box,
  Heading,
  Image,
  Stack,
  Text,
  Icon,
  SimpleGrid,
  Center,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { IoIosArrowRoundForward } from 'react-icons/io';
import ReactPlayer from 'react-player/youtube';

import Section from '../components/sections/Section';
import Hero from '../components/sections/Hero';
import Layout from '../components/layouts';

import alamImages from '../assets/images/tempat-section-images.png';
import kulinerImages from '../assets/images/kuliner-section-images.png';
import budayaImages from '../assets/images/budaya-section-images.png';

const Landing = () => {

  return (
    <Layout>

      <Hero />

      <Section>
        <Stack id="mulai-sekarang-target" textAlign="center">
          <Text w="100%" fontWeight="extra_bold" color="brand.primary">
            KEINDAHAN PASURUAN
          </Text>
          <Heading fontWeight="extra_bold">
          Keindahan & Pesona Pasuruan
          </Heading>
          <Text className="col-md-7 mx-auto">
            
Nikmati berbagai macam tempat, kuliner dan keindahan alam yang membuat-mu jatuh cinta dan menjadikan Kota Pasuruan sebagai destinasi-mu selanjutnya
          </Text>
        </Stack>

        <SimpleGrid
        mt={24}
          columns={[1, 1, 1, 2]}
          alignItems="center"
          spacing={['1rem', '1rem', '1rem', '5rem']}
        >
          <Center>
          <Image src={alamImages} w="416px" objectFit="cover" />
          </Center>
          <Stack spacing="1rem" align="start">
            <Text w="100%" fontWeight="extra_bold" color="brand.primary">
            KEINDAHAN KOTA PASURUAN
            </Text>
            <Heading
              fontWeight="extra_bold"
              fontSize={['30px', '30px', '30px', '40px']}
              lineHeight="48px"
            >
              Keindahan tempat yang bisa anda nikmati hanya di Kota Pasuruan!
            </Heading>
            <Text textAlign="justify" fontSize="18px">
            Banyak tempat yang bisa kalian temukan di Kota Pasuruan. Mulai dari Wisata Bahari, Religi, Cagar Budaya, Taman dan Tempat bersejarah  yang bisa kalian nikmati di Kota Pasuruan ini.
            </Text>
            <Text
              color="#007C49"
              fontWeight="extra_bold"
              colorScheme="green"
              variant="brand.primary"
            >
              <Link to="/eksplorasi?type=1" style={{ textDecoration: 'none' }}>
                Selengkapnya
                <Icon as={IoIosArrowRoundForward} boxSize="2rem" ml="4px" />
              </Link>
            </Text>
          </Stack>
        </SimpleGrid>

        <SimpleGrid
        mt={24}
          columns={[1, 1, 1, 2]}
          alignItems="center"
          spacing={['1rem', '1rem', '1rem', '5rem']}
        >
          <Stack spacing="1rem" align="start">
            <Text w="100%" fontWeight="extra_bold" color="brand.primary">
            KEBERAGAMAN KOTA PASURUAN
            </Text>
            <Heading
              lineHeight="48px"
              fontWeight="extra_bold"
              fontSize={['30px', '30px', '30px', '40px']}
            >
              Keberagaman budaya yang bisa anda nikmati hanya di Kota Pasuruan!
            </Heading>
            <Text fontSize="18px" textAlign="justify">
            Beragam budaya bisa kalian temukan di Kota Pasuruan. Mulai dari Pencak Silat, Tari Tradisional dan Tradisi Tradisional yang bisa kalian nikmati di Kota Pasuruan.
            </Text>

            <Text
              color="#007C49"
              fontWeight="extra_bold"
              colorScheme="green"
              variant="ghost"
            >
              <Link to="/eksplorasi?type=2">
                Selengkapnya
                <Icon as={IoIosArrowRoundForward} boxSize="2rem" ml="4px" />
              </Link>
            </Text>
          </Stack>
          <Center>
          <Image src={budayaImages} w="416px" objectFit="cover" />
          </Center>
        </SimpleGrid>

        <SimpleGrid
        mt={24}
          columns={[1, 1, 1, 2]}
          alignItems="center"
          spacing={['1rem', '1rem', '1rem', '5rem']}
        >
          <Center>
            <Image src={kulinerImages} w="416px" objectFit="cover" />
          </Center>
          <Stack spacing="1rem" align="start">
            <Text w="100%" fontWeight="extra_bold" color="brand.primary">
            KULINER KOTA PASURUAN
            </Text>
            <Heading
              lineHeight="48px"
              fontWeight="extra_bold"
              fontSize={['30px', '30px', '30px', '40px']}
            >
              Kenikmatan kuliner pasuruan yang siap menggoyang lidah
            </Heading>
            <Text textAlign="justify" fontSize="18px">
            Kota Pasuruan memiliki banyak ragam kuliner yang bisa kalian coba mulai dari minuman dan makanan dengan kelezatan yang nikmat hanya di Kota Pasuruan.
            </Text>
            <Text
              color="#007C49"
              fontWeight="extra_bold"
              colorScheme="green"
              variant="ghost"
            >
              <Link to="/eksplorasi?type=3">
                Selengkapnya
                <Icon as={IoIosArrowRoundForward} boxSize="2rem" ml="4px" />
              </Link>
            </Text>
          </Stack>
        </SimpleGrid>
      </Section>

      <Section>
        <Box>
          <Stack textAlign="center">
            <Text w="100%" fontWeight="extra_bold" color="brand.primary">
            VIDEO SEKILAS
            </Text>
            <Heading fontWeight="extra_bold">
            Ketahui Pasuruan Lewat Video Pendek
            </Heading>
            <Text className="col-md-8 mx-auto">
            Video ini berisi tempat tempat wisata yang ada di Kota Pasuruan, dibuat oleh tim Ramapati radio kebanggaan warga Kota Pasuruan
            </Text>
          </Stack>
          <ReactPlayer
            style={{
              margin: '30px auto 0 auto',
            }}
            width="full"
            height="560px"
            url="https://www.youtube.com/watch?v=54qdByicXLk"
          />
        </Box>
      </Section>
      {/* <Sponsor /> */}
    </Layout>
  );
};

export default Landing;