import React from 'react';
import { Icon } from '@chakra-ui/react';
import {
  FaInstagram,
  FaTwitter,
  FaHome,
  FaPhoneAlt,
  FaFacebook,
} from 'react-icons/fa';
import { MdEmail } from 'react-icons/md';

import banner from '../assets/images/banner.png';

import Layout from '../components/layouts';
import Banner from '../components/sections/Banner';

const Tentang = () => {
  return (
    <Layout>
      <Banner
        title="Tentang"
        description="Beranda &nbsp; →  &nbsp; Tentang"
        image={banner}
      />
      <section className="contact_area mt-5">
        <div className="container">
    
          <div className="row mt-4">
            <div className="col-md-6">
              <div className="contact_info">
                <div className="info_item">
                  <h6>
                    <Icon
                      color="brand.primary"
                      as={FaHome}
                      mr="0.5rem"
                      fontSize="lg"
                    />{' '}
                    Jl. Pahlawan No.28 Kota Pasuruan, Jawa Timur
                  </h6>
                  <p>
                    Diatas merupakan alamat kami, jika anda mempunyai
                    kepentingan anda dapat menuju alamat tersebut.
                  </p>
                </div>
                <div className="info_item mt-3">
                  <h6>
                    <Icon
                      color="brand.primary"
                      as={FaPhoneAlt}
                      mr="0.5rem"
                      fontSize="lg"
                    />
                    0888925834753
                  </h6>
                  <p>
                    Anda dapat menghubungi kami lewat telepon jika kami tidak
                    merespon pesan anda lewat fitur kontak di website ini.
                  </p>
                </div>
                <div className="info_item mt-3">
                  <h6>
                    <Icon
                      color="brand.primary"
                      as={MdEmail}
                      mr="0.5rem"
                      fontSize="lg"
                    />{' '}
                    lenjellen@gmail.com
                  </h6>
                  <p>
                    Anda dapat menghubungi kami lewat email jika anda butuh
                    informasi tentang website kami.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="contact_info">
                <div className="info_item">
                  <h6>
                    <Icon
                      color="brand.primary"
                      as={FaFacebook}
                      mr="0.5rem"
                      fontSize="lg"
                    />{' '}
                    Lenjellen - Jalan Jalan ke Kota Pasuruan
                  </h6>
                  <p>
                    Anda dapat mengikuti kami di platform facebook agar anda
                    tidak kehilangan informasi terbaru tentang Lenjellen.
                  </p>
                </div>
                <div className="info_item mt-3">
                  <h6>
                    <Icon
                      color="brand.primary"
                      as={FaInstagram}
                      mr="0.5rem"
                      fontSize="lg"
                    />
                    @lenjellen
                  </h6>
                  <p>
                    Anda dapat mengikuti kami di platform instagram agar anda
                    tidak kehilangan informasi terbaru tentang Lenjellen.
                  </p>
                </div>
                <div className="info_item mt-3">
                  <h6>
                    <Icon
                      color="brand.primary"
                      as={FaTwitter}
                      mr="0.5rem"
                      fontSize="lg"
                    />{' '}
                    @lenjellen
                  </h6>
                  <p>
                    Anda dapat mengikuti kami di platform twitter agar anda
                    tidak kehilangan informasi terbaru tentang Lenjellen.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Tentang;