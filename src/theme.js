import { extendTheme } from "@chakra-ui/react"

const theme = extendTheme({
  fonts: {
    heading: "Manrope",
    body: "Manrope",
  },

  colors: {
    brand: {
      primary: "#1C7947",
      secondary: "#39A388",
      accent: "#F7A54B",
    },

    green: {
      50: '#E7F5E9',
      100: '#C8E6C9',
      200: '#A5D6A7',
      300: '#81C783',
      400: '#66BB69',
      500: '#4CAF4F',
      600: '#43A047',
      700: '#388E3C',
      800: '#2F7C31',
      900: '#1A5E20',
    },
  },

  fontSizes: {
    xs: '14px',
    sm: '16px',
    md: '18px',
    lg: '20px',
    xl: '24px',
    '2xl': '28px',
    '3xl': '36px',
    '4xl': '48px',
    '5xl': '64px',
    '6xl': '72px',
  },
  fontWeights: {
    regular: 400,
    medium: 500,
    semi_bold: 600,
    bold: 700,
    extra_bold: 800,
  },

})

export default theme