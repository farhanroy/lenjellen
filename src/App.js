import "@fontsource/manrope/400.css";
import "@fontsource/manrope/500.css";
import "@fontsource/manrope/600.css";
import "@fontsource/manrope/700.css";
import "@fontsource/manrope/800.css";
import 'bootstrap/dist/css/bootstrap.min.css';

import * as React from "react"
import { ChakraProvider } from "@chakra-ui/react"
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';

import theme from './theme';
import Home from './pages/Home'
import WisataDaerah from "./pages/WisataDaerah";
import Tentang from "./pages/Tentang";
import PetaWisata from "./pages/PetaWisata";
import Kontak from "./pages/Kontak";
import EventKota from "./pages/EventKota";

import './stylesheets/html.css';

function App() {

  return (
    <Provider store={store}>
      <ChakraProvider theme={ theme }>
        <Router>
          <Switch>
            <Route path="/" component={ Home } exact/>
            <Route path="/wisata-daerah" component={ WisataDaerah } exact/>
            <Route path="/event-kota" component={ EventKota } exact/>
            <Route path="/peta-wisata" component={ PetaWisata } exact/>
            <Route path="/tentang" component={ Tentang } exact/>
            <Route path="/kontak" component={ Kontak } exact/>
          </Switch>
        </Router>
      </ChakraProvider>
    </Provider>
  )
}

export default App;
