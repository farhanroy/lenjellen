import React, { useRef, useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

import '../../stylesheets/map.css';

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;

// eslint-disable-next-line import/no-webpack-loader-syntax
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;

const Mapgl = () => {
   

    return (
        <div>
            
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31634.356986945575!2d112.9050948!3d-7.651435249999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7c5e1a5fb57df%3A0xbe8c958dde768096!2sPasuruan%2C%20Kota%20Pasuruan%2C%20Jawa%20Timur!5e0!3m2!1sid!2sid!4v1654654605018!5m2!1sid!2sid"
                width="100%"
                height={550}
                style={{ border: 0 }}
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
                />

        </div>
    );
};

export default Mapgl;