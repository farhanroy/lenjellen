import React from 'react';
import {
  Box,
  Heading,
  Icon,
  Image,
  Link,
  SimpleGrid,
  Stack,
  Text,
} from '@chakra-ui/react';
import { FaInstagram, FaTwitter, FaFacebook, FaYoutube } from 'react-icons/fa';
import { FiMail, FiPhone, FiMapPin } from 'react-icons/fi';
import { Link as RouterLink } from 'react-router-dom';

import Logo from '../../assets/logo/logo.png';

const IconSosmed = ({ icon, url }) => (
  <Link href={url} isExternal>
    <Icon color="brand.primary" as={icon} fontSize="lg" />
  </Link>
);

const LinkItem = ({ to, children, ...rest }) => (
  <Link href={to}>{children}</Link>
);

const Footer = () => {
  return (
    <Box p={['2rem', '2rem', '3rem', '5rem']}>
      <SimpleGrid columns={[1, 1, 1, 4]} spacing="3rem">
        <Stack spacing="1rem" align="start">
          <RouterLink to="/">
            {' '}
            <Image src={Logo} width={10} />
          </RouterLink>
          <Text textAlign="justify" fontSize="14px">
          Lenjellen adalah platform web yang mengekspos wisata, budaya, dan kuliner yang ada di kota pasuruan agar warga kota pasuruan atau yang diluar daerah bisa tahu destinasi apa saja yang bisa dikunjungi di Kota Pasuruan
          </Text>
          <Stack direction="row" spacing="1rem">
            <IconSosmed
              url="https://www.facebook.com/"
              icon={FaFacebook}
            />
            <IconSosmed url="https://twitter.com/" icon={FaTwitter} />
            <IconSosmed
              url="https://www.youtube.com/"
              icon={FaYoutube}
            />
          </Stack>
        </Stack>

        <Stack justify="space-between">
          <Heading fontSize="lg">Kategori Wisata</Heading>
          <LinkItem to="/">Pantai</LinkItem>
          <LinkItem to="/">Sejarah</LinkItem>
          <LinkItem to="/">Taman</LinkItem>
          <LinkItem to="/">Kuliner</LinkItem>
        </Stack>

        <Stack justify="space-between">
          <Heading fontSize="lg">Menu Lenjellen</Heading>
          <LinkItem to="/">Beranda</LinkItem>
          <LinkItem to="wisata-daerah">Wisata Daerah</LinkItem>
          <LinkItem to="event-kota">Event Kota</LinkItem>
          <Link href="peta-wisata">
            Peta Wisata
          </Link>
          <LinkItem to="about">Tentang</LinkItem>
        </Stack>

        <Stack spacing="1rem">
          <Heading fontSize="lg">Hubungi Kami</Heading>
          <LinkItem to="mailto:app@lenjellen.com">
            <Icon color="brand.primary" as={FiMail} mr="1rem" fontSize="lg" />
            lenjellen@gmail.com
          </LinkItem>
          <LinkItem to="callto:0888925834753">
            <Icon color="brand.primary" as={FiPhone} mr="1rem" fontSize="lg" />
            0888925834753
          </LinkItem>
          <Link>
            <Text textAlign="justify">
              <Icon color="brand.primary" as={FiMapPin} mr="1rem" fontSize="lg" />
              Jl. Pahlawan No.28, Pekuncen, Panggungrejo, Kota Pasuruan, Jawa Timur 67126
            </Text>
          </Link>
        </Stack>
      </SimpleGrid>
    </Box>
  );
};

export default Footer;