import React from 'react';
import heroImages from '../../assets/images/hero-section-images.png';
import {
  Button,
  SimpleGrid,
  Text,
  Heading,
  Box,
  Image,
  Stack,
  Link,
  Center
} from '@chakra-ui/react';

const Hero = props => {
  return (
    <Box
      p={['5rem 2rem', '5rem 2rem', '5rem 2rem', '5rem 0 5rem 5rem']}
      h={['100vh', '100vh', '100vh', 'auto']}
    >
      <SimpleGrid alignItems="center" columns={[1, 1, 1, 2]}>
        <Stack spacing={5} align="start">
          <Text w="100%" fontWeight="extra_bold" color="brand.primary">
            SELAMAT DATANG DI KOTA MADINAH
          </Text>
          <Heading
            w={['auto', 'auto', 'auto', '80%']}
            fontSize={['28px', '28px', '28px', '44px']}
            letterSpacing="-1px"
            fontWeight="extra_bold"
          >
            Temukan keindahan tempat wisata di Kota Pasuruan.
          </Heading>
          <Text
            w={['100%', '100%', '100%', '80%']}
            color="#666666"
            textAlign="start"
          >
            Lenjellen membantu kamu untuk memilih tempat wisata di Kota  Pasuruan  yang menyuguhkan berbagai tempat wisata yang menarik dan wajib kamu kunjungi
          </Text>

          <SimpleGrid w={['full', 'auto']} spacing="8px" columns={[1, 2]}>
            <Link href="#mulai-sekarang-target">
              <Button
                px="4rem"
                size="lg"
                lineHeight="32px"
                letterSpacing="0.3px"
                fontWeight="extra_bold"
                fontSize="15px"
                colorScheme="whatsapp"
                isFullWidth={{ base: true, md: false }}
              >
                Mulai sekarang &nbsp;→
              </Button>
            </Link>
          
          </SimpleGrid>
        </Stack>
        <Box d={['none', 'none', 'none', 'block']}>
          <Center>
            <Image
              src={heroImages}
              size="xl"
              alt="Hero Section Images Lenjellen"
            />
          </Center>
        </Box>
      </SimpleGrid>
    </Box>
  );
};

export default Hero;